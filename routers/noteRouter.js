const express = require('express');
const router = new express.Router();
const authMiddlware = require('../middlewares/authMiddlware');
const {Note} = require('../models/noteModel');

router.post('/', authMiddlware, async (req, res) => {
  const note = new Note({
    userId: req.user._id,
    text: req.body.text,
  });
  try {
    if (!req.body.text) {
      res.status(400).json({message: 'Can not save empty note!'});
    }
    await note.save();
    res.status(200).json({message: 'Success'});
  } catch (error) {
    res.status(400).json({message: error.message});
  }
});

router.get('/', authMiddlware, async (req, res) => {
  const {skip, limit} = req.query;
  const requestOptions = {
    skip: parseInt(skip),
    limit: parseInt(limit),
  };
  const notes = await Note.find({userId: req.user._id},
      {__v: 0}, requestOptions);
  res.status(200).json({notes: notes});
});

router.get('/:id', authMiddlware, async (req, res) => {
  const _id = req.params.id;

  const note = await Note.findOne({_id, userId: req.user._id});

  if (!note) {
    res.status(400).json({message: 'No note!'});
  }
  console.log(note);
  res.status(200).json({note: note});
});

router.put('/:id', authMiddlware, async (req, res) => {
  if (!req.body.text) {
    res.status(400).json({message: 'Wrong data!'});
  }
  const _id = req.params.id;
  const note = await Note.findOne({_id, userId: req.user._id});
  if (!note) {
    res.status(400).json({message: 'No note'});
  }
  note.text = req.body.text;
  await note.save();

  res.status(200).json({message: 'Note was updated!'});
});

router.patch('/:id', authMiddlware, async (req, res) => {
  const _id = req.params.id;
  const note = await Note.findOne({_id, userId: req.user._id});
  if (!note) {
    res.status(400).json({message: 'No note'});
  }
  await Note.updateOne(note, {completed: !note['completed']});
  res.status(200).json({message: 'Success'});
});

router.delete('/:id', authMiddlware, async (req, res) => {
  const _id = req.params.id;
  const note = await Note.findOne({_id, userId: req.user._id});
  if (!note) {
    res.status(400).json({message: 'No note!'});
  }
  await Note.findOneAndDelete({
    _id,
    userId: req.user._id,
  });
  res.status(200).json({message: 'Success'});
});

module.exports = router;
