const mongoose = require('mongoose');
const express = require('express');
const morgan = require('morgan');
const fs = require('fs');
const path = require('path');
const app = express();
require('dotenv').config();
const port = process.env.PORT || 8080;

const authRouter = require('./routers/authRouter');
const userRouter = require('./routers/userRouter');
const noteRouter = require('./routers/noteRouter');

app.use(express.json());
app.use(express.static('build'));
const logFile = fs.createWriteStream(path.join(__dirname, 'morgan.log'), {
  flags: 'a',
});
app.use(morgan('tiny', {stream: logFile}));

app.use('/api/auth', authRouter);
app.use('/api/users/me', userRouter);
app.use('/api/notes', noteRouter);


app.use((err, req, res, next) => {
  res.status(500).json({message: err.message});
});

const start = async () => {
  await mongoose.connect(
      'mongodb+srv://nodeJsHw:Node8359@cluster0.av8mf.mongodb.net/nodeJsHW2?retryWrites=true&w=majority',
      {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useFindAndModify: false,
        useCreateIndex: true,
      },
  );

  app.listen(port, () => {
    console.log(`Server works at port ${port}!`);
  });
};

start();
