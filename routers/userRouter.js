const express = require('express');
const router = new express.Router();
const bcrypt = require('bcrypt');
const authMiddlware = require('../middlewares/authMiddlware');
const {User} = require('../models/userModel');

router.get('/', authMiddlware, async (req, res) => {
  if (req.user) {
    res.status(200).json({user: req.user});
  } else {
    res.status(400).json({message: 'Please confirm the identity!'});
  }
});

router.delete('/', authMiddlware, async (req, res) => {
  try {
    const user = await User.findOne({_id: req.user._id});

    if (!user) {
      res.status(400).json({message: 'No user found'});
    }

    await User.deleteOne({_id: req.user._id});
    res.status(200).json({message: 'User was deleted'});
  } catch (error) {
    res.status(400).json({message: error.message});
  }
});

router.patch('/', authMiddlware, async (req, res) => {
  const {oldPassword, newPassword} = req.body;
  const user = await User.findOne({_id: req.user._id});

  if (!oldPassword || !newPassword) {
    res.status(400).json({message: 'Wrong data!'});
  }

  if (!user) {
    res.status(400).json({message: 'No user found!'});
  }

  if (!(await bcrypt.compare(oldPassword, user.password))) {
    res.status(400).json({message: 'password is not correct!'});
  }

  if (oldPassword === newPassword) {
    res.status(400).json({message: 'same passwords!'});
  }

  await User
      .updateOne(user, {$set: {password: await bcrypt.hash(newPassword, 10)}});

  res.status(200).json({message: 'Password was changed'});
});

module.exports = router;
