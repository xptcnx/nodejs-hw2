const Joi = require('joi');

module.exports.validateRegistration = async (req, res, next) => {
  const schema = Joi.object({
    username: Joi.string().alphanum().required(),
    password: Joi.string()
        .pattern(new RegExp('^[a-zA-Z0-9]{6,30}$'))
        .required(),
  });

  await schema
      .validateAsync(req.body).then(() => {
        next();
      })
      .catch((err) => {
        return res.status(400).json({message: err.message});
      });
};
