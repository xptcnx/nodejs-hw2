const jwt = require('jsonwebtoken');
const {JWT_SECRET} = require('../config');

const authMiddlware = async (req, res, next) => {
  const header = req.headers['authorization'];

  if (!header) {
    return res.status(400).json({message: 'No authorized!'});
  }

  const [tokenType, token] = header.split(' ');

  jwt.verify(token, JWT_SECRET, (err, user) => {
    if (err) {
      return res.status(400)
          .json({message: `No valid ${tokenType} token found!`});
    }
    req.user = user;
    next();
  });
};

module.exports = authMiddlware;
